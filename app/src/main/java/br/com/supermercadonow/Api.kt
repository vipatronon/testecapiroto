package br.com.supermercadonow

import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

interface Api {

    @POST("login")
    fun teste(@Body credentials: credentials): Observable<String>
}

data class credentials(
    val email: String,
    val senha: String
)