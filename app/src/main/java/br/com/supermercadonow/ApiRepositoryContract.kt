package br.com.supermercadonow

import io.reactivex.Observable

interface ApiRepositoryContract {
    fun teste(email: String, senha: String): Observable<String>
}