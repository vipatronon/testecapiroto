package br.com.supermercadonow.testedocapiroto

import android.util.Log
import br.com.supermercadonow.ApiRepositoryContract
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver

class MainModel(
    private val presenter: MainContract.Presenter,
    private val api: ApiRepositoryContract
) : MainContract.Model {

    private val disposables = CompositeDisposable()

    override fun teste() {
        api.teste("victor.shopper@gmail.com", "Cookie**1478")
            .compose(AppUtil.getNetworkThread())
            .doOnSubscribe { disposables.add(it) }
            .subscribe(object : DisposableObserver<String>() {
                override fun onComplete() {
                    Log.d("Teste", "model complete")
                    presenter.complete()
                }

                override fun onNext(t: String) {
                    Log.d("Teste", "model next")
                    presenter.next()
                }

                override fun onError(e: Throwable) {
                    Log.d("Teste", "model error")
                    presenter.error()
                }
            })
    }
}