package br.com.supermercadonow.testedocapiroto

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class MainActivity : AppCompatActivity(), MainContract.View {

    private val presenter: MainContract.Presenter by inject { parametersOf(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.start()
    }

    override fun complete() {
        Log.d("Teste", "view complete")
    }

    override fun next() {
        Log.d("Teste", "view next")
    }

    override fun error() {
        Log.d("Teste", "view error")
    }
}
