package br.com.supermercadonow.testedocapiroto

interface MainContract {
    interface View {
        fun complete()
        fun next()
        fun error()

    }
    interface Presenter {
        fun start()
        fun error()
        fun next()
        fun complete()
    }
    interface Model {
        fun teste()

    }
}