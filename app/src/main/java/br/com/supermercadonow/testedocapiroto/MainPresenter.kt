package br.com.supermercadonow.testedocapiroto

import android.util.Log
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.parameter.parametersOf

class MainPresenter(private val view: MainContract.View) : MainContract.Presenter, KoinComponent {

    private val model: MainContract.Model by inject { parametersOf(this) }

    override fun start() {

        model.teste()
    }

    override fun error() {
        Log.d("Teste", "presenter error")
        view.error()
    }

    override fun next() {
        Log.d("Teste", "presenter next")
        view.next()
    }

    override fun complete() {
        Log.d("Teste", "presenter complete")
        view.complete()
    }

}

class AppUtil {
    companion object {
        fun <T> getNetworkThread(): ObservableTransformer<T, T>? {
            return ObservableTransformer { observable: Observable<T> ->
                observable.subscribeOn(
                    Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread())
            }
        }
    }
}