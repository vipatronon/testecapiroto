package br.com.supermercadonow

import io.reactivex.Observable

class ApiRepository(private val api: Api): ApiRepositoryContract {
    override fun teste(email: String, senha: String): Observable<String> = api.teste(credentials(email, senha))
}