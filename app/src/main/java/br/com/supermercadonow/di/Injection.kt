package br.com.supermercadonow.di

object Injection {
    val modules = listOf(
        NetworkModules.instance,
        AppModules.instance
    )
}