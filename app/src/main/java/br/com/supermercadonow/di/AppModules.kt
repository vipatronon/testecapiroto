package br.com.supermercadonow.di

import br.com.supermercadonow.Api
import br.com.supermercadonow.ApiRepository
import br.com.supermercadonow.ApiRepositoryContract
import br.com.supermercadonow.testedocapiroto.MainContract
import br.com.supermercadonow.testedocapiroto.MainModel
import br.com.supermercadonow.testedocapiroto.MainPresenter
import org.koin.dsl.module
import retrofit2.Retrofit

object AppModules {
    val instance = module {
        factory<Api> { get<Retrofit>().create(Api::class.java) }

        single<ApiRepositoryContract> { ApiRepository(get()) }

        factory<MainContract.Presenter> { (view: MainContract.View) ->
            MainPresenter(view)
        }

        factory<MainContract.Model> { (presenter: MainContract.Presenter) ->
            MainModel(presenter, get())
        }
    }
}